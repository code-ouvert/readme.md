# [Nom du projet](https://nom-du-projet.dev)

Texte descriptif du projet

## Documentation
La documentation est disponible [ici](https://code-ouvert.github.io/lien-du-repository/).

## Contribuer
Veuillez prendre un moment pour lire le [guide sur la contribution](CONTRIBUTING.md).

## Changelog
[CHANGELOG.md](CHANGELOG.md) liste tous les changements effectués lors de chaque release.

## À propos
myblog.dev a été conçu initialement par [Yassin El Kamal NGUESSU](https://github.com/code-ouvert). Si vous avez le moindre question, contactez [Yassin El Kamal NGUESSU](mailto:t-boileau@email.com?subject=[Github]%20iletaitunefoisundev)


## Environnement de développement

### Pré-requis

* PHP 8.0
* Composer
* Symfony CLI
* MySQL 8.0

Vous pouvez vérifier la pré-requis avec la commande suivante (de la CLI Symfony):

```bash
symfony book:check-requirements
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
symfony serve -d
symfony open:local
```

### Lancer des tests

```bash
php bin/phpunit --testdox
```

## Plan de realisation du projet

### 01.Creation du projet

```bash
symfony new risy --full
cd risy
subl .
touch README.md
touch LICENSE
touch CONTRIBUTING.md
touch CHANGELOG.md
```

### 02.Creation du depot GitLab

```bash
git config --global user.name "CODE OUVERT"
git config --global user.email "leyassino@gmail.com"
git init
git remote add origin git@gitlab.com:code-ouvert/nameOfRepository.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

### 03.Integration continu GitLab CI

```bash
touch .gitlab-ci.yml
```

### 04.Mettre en place une base de donnee

```bash
touch .env.local
symfony console doctrine:database:create
```
Code SQL

```bash
mysql -u yassin -p
show databases;
```

### 05.Creation des classes d'entites

Entity
```bash
symfony console make:entity
```

User
```bash
symfony console make:user
```

### 06.Creation des traits

```bash

```

### 07.

```bash

```

### 08.

```bash

```

### 09.

```bash

```

### 10.

```bash

```